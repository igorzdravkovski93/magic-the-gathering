# Magic-The Gathering

Fully responsive web app created using React.js. The data for this app is fetched from https://api.magicthegathering.io/v1/cards?random=true&pageSize=100&language=English.

## Live demo

  - https://magic--the-gathering.herokuapp.com/
  
## Technologies
  - React.js
  - CSS3
  
## Setup
You need to have node.js and npm installed to run this project
```sh
git clone https://gitlab.com/igorzdravkovski93/magic-the-gathering.git
cd magic-the-gathering
npm install
npm start
```
## Routes:
```sh
- /
- /cards
```
