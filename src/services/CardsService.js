import axios from "axios";

class CardsService {

  static getCardsData() {
    return axios.get(
      "https://api.magicthegathering.io/v1/cards?random=true&pageSize=100&language=English"
    );
  }

  static getTypes() {
    return axios.get("https://api.magicthegathering.io/v1/types");
  }

  static sortData(data, param, sort) {
    return data.sort((a, b) => {
      let fa = a[param.toLowerCase()].toLowerCase();
      let fb = b[param.toLowerCase()].toLowerCase();
      if (sort.toLowerCase() === "asc") {
        if (fa < fb) return -1;
        if (fa > fb) return 1;
      }
      if (sort.toLowerCase() === "desc") {
        if (fa > fb) return -1;
        if (fa < fb) return 1;
      }
      return 0;
    });
  }

  static filterData(data, filters) {
    let result = data.filter(
      (card) =>
        card.name
          .concat(` ${card.text}`)
          .toLowerCase()
          .includes(filters.search.toLowerCase()) &&
        card.types.sort().join(",").includes(filters.type.sort().join(",")) &&
        card.colors.sort().join(",").includes(filters.color.sort().join(","))
    );
    return result;
  }

  static calculateHeight(className) {
    let cards = document.getElementsByClassName(className);
    let height = 0;
    Array.from(cards).map((card) => {
      if (card.clientHeight > height) height = card.clientHeight;
    });
    return height;
  }

  static resizeCards(className, height) {
    let cards = document.getElementsByClassName(className);
    Array.from(cards).map((card) => {
      return (card.style.height = `${height}px`);
    });
  }

  static manageDropDownElements(array, element) {
    let placeholder = document.getElementById(`selected-${element}`);
    
    let select = document.getElementById(element);

    if (placeholder) select.removeChild(placeholder);

    select.childNodes[0].innerHTML = `Deselect all`;

    let option = document.createElement("option");
    option.id = `selected-${element}`;
    option.innerHTML = array[element].join(", ");
    option.value = array[element].join(", ");
    option.selected = true;

    array[element].length > 0
      ? select.prepend(option)
      : (select.childNodes[0].innerHTML = `Choose a ${element}`);

    if (array[element].length === 0)
      document.getElementById(`${element}-default`).selected = true;
  }

  static manageActiveFilters(array, element) {
    let select = document.getElementById(element);

    let options = select.childNodes;
    Array.from(options).map((option) => {
      array[element].includes(option.value)
        ? option.classList.add("active")
        : option.classList.remove("active");
    });

    if(element !== 'sort')
      select.childNodes[0].classList.remove('active') 
  }
}

export default CardsService;
