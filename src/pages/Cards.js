import React, { useState, useEffect } from "react";

// services
import CardsService from "../services/CardsService";

// components
import Header from "../components/header";
import FilterMenu from "../components/filterMenu";
import Loader from "../components/loader";
import Card from "../components/card";
import NoResults from "../components/noResults";

const Cards = (props) => {
  const [user, setUser] = useState("");
  const [loading, setLoading] = useState(true);
  const [cardsData, setCardsData] = useState([]);
  const [types, setTypes] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [noResults, setNoResults] = useState(false);
  const [cardsHeight, setCardsHeight] = useState(0);
  const [filters, setFilters] = useState({
    search: "",
    type: [],
    color: [],
    sort: "",
  });

  const handleChange = (event) => {
    let element = event.currentTarget.id;
    let value = event.currentTarget.value;
    let filtersState = { ...filters };
    if (element === "type" || element === "color") {
      let filterArray = [...filtersState[element]];

      value === "default"
        ? (filtersState[element] = [])
        : !filterArray.includes(value)
        ? (filtersState[element] = [...filtersState[element], value])
        : (filtersState[element] = filtersState[element].filter(
            (val) => val !== value
          ));

      CardsService.manageDropDownElements(filtersState, element);
    } else {
      filtersState[element] = event.currentTarget.value;
    }
    if (element !== "search")
      CardsService.manageActiveFilters(filtersState, element);

    setFilters(filtersState);
  };

  useEffect(() => {
    if (cardsData.length > 0) {
      if (filters.sort.length > 0) {
        CardsService.sortData(
          cardsData,
          filters.sort.split(" ")[0],
          filters.sort.split(" ")[1]
        );
      }

      let result = CardsService.filterData(cardsData, filters);
      result.length > 0 ? setNoResults(false) : setNoResults(true);
      setFilteredData(result);
    }
  }, [filters]);

  useEffect(() => {
    CardsService.resizeCards("card", cardsHeight * 1.2);
  }, [filteredData]);

  useEffect(() => {
    props.location.state !== undefined && props.location.state.user
      ? setUser(props.location.state.user)
      : props.history.push({ pathname: "/" });

    CardsService.getTypes()
      .then((res) => setTypes(res.data.types))
      .catch((err) => console.log(err));

    CardsService.getCardsData()
      .then((res) => {
        if (res.data.cards) {
          let cardData = {};
          res.data.cards.map((card) => {
            cardData = {
              id: card.id,
              name: card.name,
              text: card.text,
              types: card.types,
              colors: card.colors,
              setName: card.setName,
            };
            setCardsData((data) => [...data, cardData]);
          });
          setLoading(false);
          let height = CardsService.calculateHeight("card");
          setCardsHeight(height);
          CardsService.resizeCards("card", height * 1.2);
        }
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <div>
      <Header text={user} />
      {loading ? (
        <Loader />
      ) : (
        <>
          <FilterMenu
            onChange={handleChange}
            types={types}
            search={filters.search}
          />
          <div
            className={`container ${noResults ? "justify-content-center" : ""}`}
          >
            {noResults ? (
              <NoResults className="noResults" />
            ) : filteredData.length === 0 ? (
              cardsData.map((card) => {
                return (
                  <Card
                    key={card.id}
                    name={card.name}
                    text={card.text}
                    colors={card.colors}
                    setName={card.setName}
                    types={card.types}
                  />
                );
              })
            ) : (
              filteredData.map((card) => {
                return (
                  <Card
                    key={card.id}
                    name={card.name}
                    text={card.text}
                    colors={card.colors}
                    setName={card.setName}
                    types={card.types}
                  />
                );
              })
            )}
          </div>
        </>
      )}
    </div>
  );
};

export default Cards;
