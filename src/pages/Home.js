import React, { useState } from "react";

// components
import Form from "../components/form";
import Input from "../components/input";
import Button from "../components/button";

const Home = (props) => {
  const [name, setName] = useState("");
  const [error, setError] = useState({
    length: false,
    format: false,
  });

  const handleInputChange = (event) => {
    let inputText = event.currentTarget.value;
    setName(inputText);

    let errorMessages = { ...error };

    inputText.length < 3 && inputText.length !== 0
      ? (errorMessages.length = "Name must be at least 3 characters")
      : (errorMessages.length = false);

    inputText.length > 0 && inputText[0] !== inputText[0].toUpperCase()
      ? (errorMessages.format = "First character should be Uppercase")
      : (errorMessages.format = false);

    setError(errorMessages);
  };

  const handleFormSubmit = () => {
    if (!error.length && !error.format)
      props.history.push({
        pathname: "/cards",
        state: { user: name },
      });
  };

  return (
    <div className="container justify-content-center homePage">
      <Form id="loginForm" className="d-flex flex-column">
        <h1 className="formTitle">Magic: The Gathering</h1>
        <Input
          type="text"
          inputClassName={`nameInput ${
            (error.length || error.format) && name.length > 0 ? "error" : ""
          }`}
          value={name}
          placeholder="Please enter your name"
          onChange={handleInputChange}
        />

        <p className="errorMsg">
          {" "}
          {error.format || error.length ? error.format || error.length : null}
        </p>
        <Button
          buttonText="Submit"
          id="submitBtn"
          disabled={
            error.length || error.format || name.length === 0
              ? "disabled"
              : null
          }
          onClick={handleFormSubmit}
        />
      </Form>
    </div>
  );
};

export default Home;
