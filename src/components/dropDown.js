import React from "react";

const DropDown = (props) => {
  return (
    <div className="select">
      <select id={props.id} onChange={props.onChange}>
        <option
          id={`${props.id}-default`}
          defaultChecked
          hidden={props.id === "sort" ? true : false}
          value="default"
        >
          {props.title}
        </option>
        {props.options.map((option, index) => {
          return (
            <option key={index} value={option}>
              {option}
            </option>
          );
        })}
      </select>
    </div>
  );
};

export default DropDown;
