import React from "react";

const Form = (props) => {
  return (
    <form id={props.id} className={props.className}>
      {props.children}
    </form>
  );
};

export default Form;
