import React from "react";

const Card = (props) => {
  const cardHeaderStyle = {
    borderRadius: "10px 10px 0 0",
    background:
      props.colors.length > 1
        ? `linear-gradient(to left, ${props.colors.join(", ").toLowerCase()})`
        : props.colors.length === 1
        ? props.colors.join(", ").toLowerCase()
        : "#EFF1F3",
  };

  return (
    <div className="card">
      {props.colors.length > 0 ? (
        <div style={cardHeaderStyle} className="cardHeader d-flex justify">
          <p className="setName">{props.setName}</p>
        </div>
      ) : (
        <div className="cardHeader grid d-flex justify">
          <p className="setName">{props.setName}</p>
        </div>
      )}
      <div className="cardContent">
        <h2>{props.name}</h2>
        <p>{props.text}</p>
        <div className="cardTypes">
          {props.types.map((type) => (
            <p>#{type}</p>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Card;
