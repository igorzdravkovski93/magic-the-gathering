import React from "react";

const Button = (props) => {
  return (
    <button id={props.id} disabled={props.disabled} onClick={props.onClick}>
      {props.buttonText}
    </button>
  );
};

export default Button;
