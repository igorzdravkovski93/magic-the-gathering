import React from "react";

const Header = (props) => {
  return (
    <div id="header">
      <p>Hello, {props.text}</p>
    </div>
  );
};

export default Header;
