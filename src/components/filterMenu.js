import React from "react";

// components
import Input from "./input";
import DropDown from "./dropDown";

const FilterMenu = (props) => {
  return (
    <div id="filterContainer" className="d-flex">
      <Input
        inputClassName="searchBar"
        placeholder="Search..."
        onChange={props.onChange}
        value={props.search}
        id="search"
      />
      <DropDown
        options={props.types}
        onChange={props.onChange}
        title="Choose a type"
        id="type"
      />
      <DropDown
        options={["White", "Blue", "Black", "Red", "Green"]}
        title="Choose a color"
        onChange={props.onChange}
        id="color"
      />
      <DropDown
        options={["Name ASC", "Name DESC"]}
        title="Sort by"
        onChange={props.onChange}
        id="sort"
      />
    </div>
  );
};

export default FilterMenu;
