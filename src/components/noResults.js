import React from "react";

import noResultsImg from "../assets/img/no-search-found.png";

const NoResults = (props) => {
  return (
    <div>
      <img src={noResultsImg} className={props.className} alt="No Results"/>
    </div>
  );
};

export default NoResults;
