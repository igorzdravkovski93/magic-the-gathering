import React from "react";

const Input = (props) => {
  return (
    <input
      type={props.type}
      className={props.inputClassName || ""}
      onChange={props.onChange}
      value={props.value}
      placeholder={props.placeholder}
      id={props.id}
    />
  );
};

export default Input;
