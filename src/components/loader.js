import React from "react";

const Loader = () => {
  return (
    <div className="container justify-content-center">
      <div id="loader"></div>
    </div>
  );
};

export default Loader;
